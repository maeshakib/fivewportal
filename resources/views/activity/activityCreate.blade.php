@extends('layouts.master')
@include('navbar.header')
@section('content')
@include('sidebar.dashboard')
<link rel="stylesheet" href="{{URL::to('assets/css/profile.css')}}">
<main class="col bg-faded py-3 flex-grow-1">

   
       
       

 <div row="row">
 <form action="{{ route('activity/store') }}" method="POST" class="form-horizontal">
            @csrf
         
<fieldset>

<!-- Form Name -->
<legend>Activity Edit</legend>



<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Sub Sector</label>
  <div class="col-md-8">
  <select name="SubSector" id="SubSector" class="form-control input-lg dynamic" data-dependent="ActivityCategory">

     <option value="0">Select Sub Sector</option>
     @foreach($activities as $country)
     <option value="{{ $country->SubSector}}" >{{ $country->SubSector }}</option>
     @endforeach
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Activity Category</label>
  <div class="col-md-8">
  <select name="ActivityCategory" id="ActivityCategory" class="form-control input-lg dynamic" data-dependent="activity">
     <option value="0">Select Activity Category</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Activity</label>
  <div class="col-md-8">
  <select name="activity" id="activity" class="form-control input-lg">
     <option value="0">Select Activity</option>
    </select>
  </div>
</div>



<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">Actiivity Indicator</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="textarea" name="ActiivityIndicator"></textarea>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">Output Indicator</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="textarea" name="OutputIndicator"></textarea>
  </div>
</div>

<div class="col-sm-6" style="margin-top:19px;text-align:right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

   {{ csrf_field() }}

</fieldset>
</form>


</div>
    
           
       
 

</main>

<script>
$(document).ready(function(){
 
 $('.dynamic').change(function(){
  if($(this).val() != '')
  {
   var select = $(this).attr("id");
   var value = $(this).val();
   var dependent = $(this).data('dependent');
   var _token = $('input[name="_token"]').val();
   $.ajax({
    url:"{{ route('dynamicdependent.fetch') }}",
    method:"POST",
    data:{select:select, value:value, _token:_token, dependent:dependent},
    success:function(result)
    {
     $('#'+dependent).html(result);
    }
 
   })
  }
 });
 
 $('#country').change(function(){
  $('#state').val('');
  $('#city').val('');
 });
 
 $('#state').change(function(){
  $('#city').val('');
 });
 
 
});
</script>
@endsection