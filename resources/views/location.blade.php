@extends('layouts.master')
@include('navbar.header')
@section('content')
@include('sidebar.dashboard')
<link rel="stylesheet" href="{{URL::to('assets/css/profile.css')}}">
<main class="col bg-faded py-3 flex-grow-1">

   
       
       
    <div id="table"  >
                <div row="row">
    <table class="table table-striped " style="margin-top:20px;">
        <thead style="background-color:#0070BF; color:white;">
            <tr>
             

                <th>Location_Type</th>
                <th>Location_Name</th>
                <th>Reference_zone</th>

                <th>Location ID</th>
              
                <th>Action</th>    


            </tr>
        </thead>
        @foreach ($Location as $product)
        <tr>
           
            <td>{{ $product->Location_Type }}</td>
            <td>{{ $product->Location_Name }}</td>


            <td>
                <p>{{ $product->Reference_zone }}</p>
            </td>
            <td>
                <p>{{ $product->locationid  }}</p>
            </td>
            
      



            <td>
            <a class="btn btn-success" href="#">Show</a>    
            
           <a class="btn btn-primary" href="#">Edit</a>
      
                
            </td>
        </tr>
        @endforeach
    </table>

</div>
                </div>
           
       
 

</main>
@endsection