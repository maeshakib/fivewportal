<aside class="col-12 col-md-2 p-0 bg-dark flex-shrink-1">
    <nav class="navbar navbar-expand navbar-dark bg-dark flex-md-column flex-row align-items-start py-2">
        <div class="collapse navbar-collapse ">
            <ul class="flex-md-column flex-row navbar-nav w-100 justify-content-between">
                <li class="nav-item active">
                    <a class="nav-link pl-0 text-nowrap" href="{{ route('home/page') }}"><i class="fa fa-bullseye fa-fw"></i> <span class="font-weight-bold">WHAT - Activity</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-0" href="{{ route('organization/page') }}"><i class="fa fa-book fa-fw"></i> <span class="d-none d-md-inline">WHO - Organization </span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link pl-0" href="{{ route('Location/page') }}"><i class="fa fa-cog fa-fw"></i> <span class="d-none d-md-inline">WHERE - Location </span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link pl-0" href="{{ route('BeneficiaryType/page') }}"><i class="fa fa-heart codeply fa-fw"></i> <span class="d-none d-md-inline">WHOM - Typeof Beneficiary Table</span></a>
                </li>
              

                <li class="nav-item active">
                    <a class="nav-link pl-0 text-nowrap" href="{{ route('home/page') }}"><i class="fa fa-bullseye fa-fw"></i> <span class="font-weight-bold">WHEN - Activity Date</span></a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link pl-0 text-nowrap" href="{{ route('home/page') }}"><i class="fa fa-bullseye fa-fw"></i> <span class="font-weight-bold">WHOM - Beneficiaries</span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link pl-0" href="#"><i class="fa fa-sign-out"></i> <span class="d-none d-md-inline">Logout</span></a>
                </li>

            </ul>
        </div>
    </nav>
</aside>