@extends('layouts.master')
@include('navbar.header')
@section('content')
@include('sidebar.dashboard')
<link rel="stylesheet" href="{{URL::to('assets/css/profile.css')}}">
<main class="col bg-faded py-3 flex-grow-1">

   
       
       
    <div id="table"  >
                <div row="row">
    <table class="table table-striped " style="margin-top:20px;">
        <thead style="background-color:#0070BF; color:white;">
            <tr>
             

                <th>Organization_Name</th>
                <th>Organization_Code</th>
                <th>Organization_Type</th>

                <th>Focal_Person_Name</th>
                <th>Email</th>
                <th>Contact</th>    
                <th>Action</th>    


            </tr>
        </thead>
        @foreach ($Organization as $product)
        <tr>
           
            <td>{{ $product->Organization_Name }}</td>
            <td>{{ $product->Organization_Code }}</td>


            <td>
                <p>{{ $product->Organization_Type }}</p>
            </td>
            <td>
                <p>{{ $product->Focal_Person_Name  }}</p>
            </td>
            <td>
                <p>{{ $product->Email}}</p>
            </td>
            
            <td>
                <p>{{ $product->Contact}}</p>
            </td>


            <td>
            <a class="btn btn-success" href="#">Show</a>    
            
           <a class="btn btn-primary" href="{{ route('organization/edit',$product->id) }}">Edit</a>
      
                
            </td>
        </tr>
        @endforeach
    </table>

</div>
                </div>
           
       
 

</main>
@endsection