@extends('layouts.master')
@include('navbar.header')
@section('content')
@include('sidebar.dashboard')
<link rel="stylesheet" href="{{URL::to('assets/css/profile.css')}}">
<main class="col bg-faded py-3 flex-grow-1">

   
       
       
    <div id="table"  >
                <div row="row">
                <a class="btn btn-outline-info" href="{{ route('activity/create') }}">Create</a>    
    <table class="table table-striped " style="margin-top:20px;">
        <thead style="background-color:#0070BF; color:white;">
            <tr>
             

                <th>Sector</th>
                <th>Activity Category</th>
                <th>activity</th>

                <th>Actiivity indicator</th>
                <th>Output indicator</th>
                <th>Action</th>    


            </tr>
        </thead>
        @foreach ($Activities as $product)
        <tr>
           
            <td>{{ $product->SubSector }}</td>
            <td>{{ $product->ActivityCategory }}</td>


            <td>
                <p>{{ $product->activity }}</p>
            </td>
            <td>
                <p>{{ $product->actiivity_indicator  }}</p>
            </td>
            <td>
                <p>{{ $product->output_indicator}}</p>
            </td>
      



            <td>
            <a class="btn btn-success" href="{{ route('activity/show',$product->id) }}">Show</a>    
            
           <a class="btn btn-primary" href="{{ route('activity/edit',$product->id) }}">Edit</a>
      
          
            </td>
        </tr>
        @endforeach
    </table>

</div>
                </div>
           
       
 

</main>
@endsection