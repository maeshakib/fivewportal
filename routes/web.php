<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
    return view('login');
})->name('/');

// Home
Route::get('home/page',[App\Http\Controllers\ActivitiesController::class,'index'])->name('home/page');
Route::get('activity/create',[App\Http\Controllers\ActivitiesController::class,'create'])->name('activity/create');
Route::post('activity/store',[App\Http\Controllers\ActivitiesController::class,'store'])->name('activity/store');

Route::get('activity/show/{id}',[App\Http\Controllers\ActivitiesController::class,'show'])->name('activity/show');
Route::get('activity/edit/{id}',[App\Http\Controllers\ActivitiesController::class,'edit'])->name('activity/edit');
Route::put('activity/update/{id}',[App\Http\Controllers\ActivitiesController::class,'update'])->name('activity/update');

Route::post('dynamic_dependent/fetch', [App\Http\Controllers\ActivitiesController::class,'fetch'])->name('dynamicdependent.fetch');

//organization
Route::get('organization/page',[App\Http\Controllers\OrganizationController::class,'index'])->name('organization/page');
Route::get('organization/edit/{id}',[App\Http\Controllers\OrganizationController::class,'edit'])->name('organization/edit');

Route::get('Location/page',[App\Http\Controllers\LocationController::class,'index'])->name('Location/page');
Route::get('BeneficiaryType/page',[App\Http\Controllers\BeneficiaryTypeController::class,'index'])->name('BeneficiaryType/page');


 