<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Activities;
use Validator;

class ActivitiesController extends Controller
{
    public function index()
    {
        $Activities = Activities::all();

        return view('home')->with(compact('Activities'));
       
    }

    

    public function show($product)
    {

        $activityData = Activities::where('id',$product)->first();

       

        return view('activityShow')->with(compact('activityData'));
   
     
    }

    public function create()
    {
       // DB::enableQueryLog(); // Enable query log

        $activities = DB::table('activities')
        ->select(DB::raw(' distinct `SubSector` '))
       
        ->get();
   
       // dd(DB::getQueryLog()); // Show results of log
      //   $activityData = Activities::where('id',$id)->first();
        

        return view('activity/activityCreate')->with(compact('activities'))
;

    }







    public function store(Request $request)
    {
    //return $request;
       
        // return $single_obj;
      $rules = array(
        'SubSector' => 'required',
        'ActivityCategory' => 'required',
        'activity' => 'required',         
        'ActiivityIndicator' => 'required',
        'OutputIndicator' => 'required',    

    );
    $messages = array(
        'SubSector.required' => 'Sub Sector is Required',
        'ActivityCategory.required' => 'Activity Category is Required',
        'activity.required' => 'Activity is Required',
        'ActiivityIndicator.required' => 'Actiivity Indicator is Required',
        'OutputIndicator.required' => 'Output Indicator is Required',


    );
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route("activity/update")
                ->withErrors($validator)
                ->withInput();
        }
        //validation end here

       
  
        //new Role creation
        //return $request;
        $activitObj = new Activities;
        $activitObj->SubSector = $request->SubSector;
        $activitObj->ActivityCategory = $request->ActivityCategory;
        $activitObj->activity = $request->activity;
        $activitObj->actiivity_indicator = $request->ActiivityIndicator;
        $activitObj->output_indicator = $request->OutputIndicator;
        //return $product;
        $saved = $activitObj->save();
if(!$saved){
    return redirect()->route("activity/update")
    ->with('success', 'NO Update found.');
   
}else{
    
    return redirect()->route('home/page')
    ->with('success', 'Activity has been updated successfully.');
    // return redirect()->route('products.index')
    // ->with('success', 'Case has been updated successfully.');
}

    }












    public function edit($id)
    {
       // DB::enableQueryLog(); // Enable query log

        $activities = DB::table('activities')
        ->select(DB::raw(' distinct `SubSector` '))   ->get();

        
        $ActivityCategory_list = DB::table('activities')
        ->select(DB::raw(' distinct `ActivityCategory` '))    ->get();

        $activity_list = DB::table('activities')
        ->select(DB::raw(' distinct `activity` '))    ->get();
   
       // dd(DB::getQueryLog()); // Show results of log
         $activityData = Activities::where('id',$id)->first();
     

        return view('activity/activityEdit')->with(compact('activityData','activities','ActivityCategory_list','activity_list','id'))
;

    }


    function fetch(Request $request)
    {
      $select = $request->get('select');
       $value = $request->get('value');
        $dependent = $request->get('dependent');
     $data = DB::table('activities')
     ->select(DB::raw($dependent))
       ->where($select, $value)
       ->groupBy($dependent)
       ->get();
     $output = '<option value="">Select '.ucfirst($dependent).'</option>';
     foreach($data as $row)
     {
      $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
     }
     echo $output;
    }






    public function update(Request $request,$single_obj)
    {
    // return $request;
       
        // return $single_obj;
      $rules = array(
        'SubSector' => 'required',
        'ActivityCategory' => 'required',
        'activity' => 'required',         
        'ActiivityIndicator' => 'required',
        'OutputIndicator' => 'required',    

    );
    $messages = array(
        'SubSector.required' => 'Sub Sector is Required',
        'ActivityCategory.required' => 'Activity Category is Required',
        'activity.required' => 'Activity is Required',
        'ActiivityIndicator.required' => 'Actiivity Indicator is Required',
        'OutputIndicator.required' => 'Output Indicator is Required',


    );
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->route("activity/update")
                ->withErrors($validator)
                ->withInput();
        }
        //validation end here

       
  
        //new Role creation
        //return $request;
        $activitObj = Activities::findOrFail($single_obj);
        $activitObj->SubSector = $request->SubSector;
        $activitObj->ActivityCategory = $request->ActivityCategory;
        $activitObj->activity = $request->activity;
        $activitObj->actiivity_indicator = $request->ActiivityIndicator;
        $activitObj->output_indicator = $request->OutputIndicator;
        //return $product;
        $saved = $activitObj->save();
if(!$saved){
    return redirect()->route("activity/update")
    ->with('success', 'NO Update found.');
   
}else{
    
    return redirect()->route('home/page')
    ->with('success', 'Activity has been updated successfully.');
    // return redirect()->route('products.index')
    // ->with('success', 'Case has been updated successfully.');
}

           



    }












}
