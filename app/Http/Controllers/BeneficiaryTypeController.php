<?php

namespace App\Http\Controllers;

use App\Models\BeneficiaryType;
use Illuminate\Http\Request;

class BeneficiaryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $BeneficiaryType = BeneficiaryType::all();

        return view('beneficiarytype')->with(compact('BeneficiaryType'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BeneficiaryType  $beneficiaryType
     * @return \Illuminate\Http\Response
     */
    public function show(BeneficiaryType $beneficiaryType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BeneficiaryType  $beneficiaryType
     * @return \Illuminate\Http\Response
     */
    public function edit(BeneficiaryType $beneficiaryType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BeneficiaryType  $beneficiaryType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BeneficiaryType $beneficiaryType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BeneficiaryType  $beneficiaryType
     * @return \Illuminate\Http\Response
     */
    public function destroy(BeneficiaryType $beneficiaryType)
    {
        //
    }
}
