<?php

namespace App\Http\Controllers;
use App\Models\Location;

use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function index()
    {
        $Location = Location::all();

        return view('location')->with(compact('Location'));
    }

}
